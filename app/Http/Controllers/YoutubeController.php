<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Google_Client;
use GuzzleHttp\Client;
use Google_Service_YouTube;
use Google_Service_YouTube_LiveBroadcast;
use Google_Service_YouTube_LiveBroadcastContentDetails;
use Google_Service_YouTube_LiveBroadcastSnippet;
use Google_Service_YouTube_LiveBroadcastStatus;
use Illuminate\Support\Facades\Redirect;

class YoutubeController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = new Google_Client();
    }

    public function authGoogleAccount()
    {
        $this->client->setApplicationName('My Project 56260 youtube');
        $this->client->setAccessType("offline");
        $this->client->setAuthConfig(__DIR__ . '/client_secret.json');
        $this->client->setScopes([
            'https://www.googleapis.com/auth/youtube',
            'https://www.googleapis.com/auth/youtube.readonly',
        ]);
        $this->client->setLoginHint('cristianjavaprogrammer@gmail.com');
        $authUrl = $this->client->createAuthUrl();

        return Redirect::to($authUrl);
    }

    public function insertLiveStreaming(Request $request)
    {
        $code = $request->input('code');
        $client = new Client();
        $response = $client->request('POST', 'https://accounts.google.com/o/oauth2/token', [
            'body' => '{
                "code":"'.$code.'",
                "client_id":"270335976195-n0i0c6g53jtt7cofqjo6adoefkot9isc.apps.googleusercontent.com",
                "client_secret":"bCplZGeC9Jz10p7QuMDXOJRK",
                "redirect_uri":"https://cristiancode.com/google-callback",
                "grant_type":"authorization_code"
            }',
        ]);

        $body_response = json_decode($response->getBody());

        $this->client->setAccessToken($body_response->access_token);

        // Define service object for making API requests.
        $service = new Google_Service_YouTube($this->client);

        // Define the $liveBroadcast object, which will be uploaded as the request body.
        $liveBroadcast = new Google_Service_YouTube_LiveBroadcast();

        // Add 'contentDetails' object to the $liveBroadcast object.
        $liveBroadcastContentDetails = new Google_Service_YouTube_LiveBroadcastContentDetails();
        $liveBroadcastContentDetails->setEnableClosedCaptions(true);
        $liveBroadcastContentDetails->setEnableContentEncryption(true);
        $liveBroadcastContentDetails->setEnableDvr(true);
        $liveBroadcastContentDetails->setEnableEmbed(true);
        $liveBroadcastContentDetails->setRecordFromStart(true);
        $liveBroadcastContentDetails->setStartWithSlate(true);
        $liveBroadcastContentDetails->setEnableAutoStart(true);
        $liveBroadcastContentDetails->setEnableAutoStop(true);
        $liveBroadcast->setContentDetails($liveBroadcastContentDetails);

           // Add 'snippet' object to the $liveBroadcast object.
           $liveBroadcastSnippet = new Google_Service_YouTube_LiveBroadcastSnippet();
           $liveBroadcastSnippet->setScheduledStartTime('2021-04-23T19:17:23Z');
           $liveBroadcastSnippet->setTitle('Test broadcast backend 2');
           $liveBroadcast->setSnippet($liveBroadcastSnippet);

          // Add 'status' object to the $liveBroadcast object.
          $liveBroadcastStatus = new Google_Service_YouTube_LiveBroadcastStatus();
          $liveBroadcastStatus->setPrivacyStatus("private");
          $liveBroadcastStatus->setSelfDeclaredMadeForKids(false);
          $liveBroadcast->setStatus($liveBroadcastStatus);

          $response = $service->liveBroadcasts->insert('snippet,contentDetails,status', $liveBroadcast);
          dd($response);
    }

    public function listLiveStreaming(Request $request)
    {
        $code = $request->input('code');
        $client = new Client();
        $response = $client->request('POST', 'https://accounts.google.com/o/oauth2/token', [
            'body' => '{
                "code":"' . $code . '",
                "client_id":"270335976195-n0i0c6g53jtt7cofqjo6adoefkot9isc.apps.googleusercontent.com",
                "client_secret":"bCplZGeC9Jz10p7QuMDXOJRK",
                "redirect_uri":"https://cristiancode.com/google-callback",
                "grant_type":"authorization_code"
            }',
        ]);

        $body_response = json_decode($response->getBody());

        $this->client->setAccessToken($body_response->access_token);

        // Define service object for making API requests.
        $service = new Google_Service_YouTube($this->client);

        // Define the $liveBroadcast object, which will be uploaded as the request body.
        $liveBroadcast = new Google_Service_YouTube_LiveBroadcast();

        // Add 'snippet' object to the $liveBroadcast object.
        /*$liveBroadcastSnippet = new Google_Service_YouTube_LiveBroadcastSnippet();
        $liveBroadcastSnippet->setScheduledStartTime('2021-04-23T19:17:23Z');
        $liveBroadcastSnippet->setTitle('Test broadcast backend 2');
        $liveBroadcast->setSnippet($liveBroadcastSnippet);*/

        // Add 'status' object to the $liveBroadcast object.
        //$liveBroadcastStatus = new Google_Service_YouTube_LiveBroadcastStatus();

        //$liveBroadcast->setStatus($liveBroadcastStatus);
        $filter = [
          'broadcastStatus' => 'all'
        ];
        $response = $service->liveBroadcasts->listLiveBroadcasts('snippet,contentDetails,status', $filter);
        dd($response);
    }
}
